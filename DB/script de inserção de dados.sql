-- Blocos
insert into blocos values('Bloco A');
insert into blocos values('Bloco B');
insert into blocos values('Bloco C');
insert into blocos values('Bloco D');
insert into blocos values('Bloco E');

-- Pavimentos: Bloco A
insert into pavimentos values('1° PAV', 1);
insert into pavimentos values('2° PAV', 1);
insert into pavimentos values('3° PAV', 1);
insert into pavimentos values('4° PAV', 1);
insert into pavimentos values('5° PAV', 1);
insert into pavimentos values('6° PAV', 1);
insert into pavimentos values('7° PAV', 1);
insert into pavimentos values('8° PAV', 1);
insert into pavimentos values('9° PAV', 1);
insert into pavimentos values('10° PAV', 1);

-- Pavimentos: Bloco B
insert into pavimentos values('1° PAV', 2);
insert into pavimentos values('2° PAV', 2);
insert into pavimentos values('3° PAV', 2);
insert into pavimentos values('4° PAV', 2);
insert into pavimentos values('5° PAV', 2);

-- Pavimentos: Bloco C
insert into pavimentos values('1° PAV', 3);
insert into pavimentos values('2° PAV', 3);
insert into pavimentos values('3° PAV', 3);
insert into pavimentos values('4° PAV', 3);
insert into pavimentos values('5° PAV', 3);

-- Pavimentos: Bloco D
insert into pavimentos values('1° PAV', 4);
insert into pavimentos values('2° PAV', 4);
insert into pavimentos values('3° PAV', 4);

-- Pavimentos: Bloco E
insert into pavimentos values('1° PAV', 5);
insert into pavimentos values('2° PAV', 5);
insert into pavimentos values('3° PAV', 5);

-- Luminaria: Bloco A 1° PAV
insert into luminarias values('01', 1);
insert into luminarias values('02', 1);
insert into luminarias values('03', 1);
insert into luminarias values('04', 1);
insert into luminarias values('05', 1);
insert into luminarias values('06', 1);
insert into luminarias values('07', 1);
insert into luminarias values('08', 1);
insert into luminarias values('09', 1);
insert into luminarias values('10', 1);

-- Luminaria: Bloco A 2° PAV
insert into luminarias values('01', 2);
insert into luminarias values('02', 2);
insert into luminarias values('03', 2);
insert into luminarias values('04', 2);
insert into luminarias values('05', 2);
insert into luminarias values('06', 2);
insert into luminarias values('07', 2);
insert into luminarias values('08', 2);
insert into luminarias values('09', 2);
insert into luminarias values('10', 2);

-- Luminaria: Bloco A 3° PAV
insert into luminarias values('01', 3);
insert into luminarias values('02', 3);
insert into luminarias values('03', 3);
insert into luminarias values('04', 3);
insert into luminarias values('05', 3);
insert into luminarias values('06', 3);
insert into luminarias values('07', 3);
insert into luminarias values('08', 3);
insert into luminarias values('09', 3);
insert into luminarias values('10', 3);

-- Luminaria: Bloco A 4° PAV
insert into luminarias values('01', 4);
insert into luminarias values('02', 4);
insert into luminarias values('03', 4);
insert into luminarias values('04', 4);
insert into luminarias values('05', 4);
insert into luminarias values('06', 4);
insert into luminarias values('07', 4);
insert into luminarias values('08', 4);
insert into luminarias values('09', 4);
insert into luminarias values('10', 4);

-- Luminaria: Bloco A 5° PAV
insert into luminarias values('01', 5);
insert into luminarias values('02', 5);
insert into luminarias values('03', 5);
insert into luminarias values('04', 5);
insert into luminarias values('05', 5);
insert into luminarias values('06', 5);
insert into luminarias values('07', 5);
insert into luminarias values('08', 5);
insert into luminarias values('09', 5);
insert into luminarias values('10', 5);

-- Luminaria: Bloco A 6° PAV
insert into luminarias values('01', 6);
insert into luminarias values('02', 6);
insert into luminarias values('03', 6);
insert into luminarias values('04', 6);
insert into luminarias values('05', 6);
insert into luminarias values('06', 6);
insert into luminarias values('07', 6);
insert into luminarias values('08', 6);
insert into luminarias values('09', 6);
insert into luminarias values('10', 6);

-- Luminaria: Bloco A 7° PAV
insert into luminarias values('01', 7);
insert into luminarias values('02', 7);
insert into luminarias values('03', 7);
insert into luminarias values('04', 7);
insert into luminarias values('05', 7);
insert into luminarias values('06', 7);
insert into luminarias values('07', 7);
insert into luminarias values('08', 7);
insert into luminarias values('09', 7);
insert into luminarias values('10', 7);

-- Luminaria: Bloco A 8° PAV
insert into luminarias values('01', 8);
insert into luminarias values('02', 8);
insert into luminarias values('03', 8);
insert into luminarias values('04', 8);
insert into luminarias values('05', 8);
insert into luminarias values('06', 8);
insert into luminarias values('07', 8);
insert into luminarias values('08', 8);
insert into luminarias values('09', 8);
insert into luminarias values('10', 8);

-- Luminaria: Bloco A 9° PAV
insert into luminarias values('01', 9);
insert into luminarias values('02', 9);
insert into luminarias values('03', 9);
insert into luminarias values('04', 9);
insert into luminarias values('05', 9);
insert into luminarias values('06', 9);
insert into luminarias values('07', 9);
insert into luminarias values('08', 9);
insert into luminarias values('09', 9);
insert into luminarias values('10', 9);

-- Luminaria: Bloco A 10° PAV
insert into luminarias values('01', 10);
insert into luminarias values('02', 10);
insert into luminarias values('03', 10);
insert into luminarias values('04', 10);
insert into luminarias values('05', 10);
insert into luminarias values('06', 10);
insert into luminarias values('07', 10);
insert into luminarias values('08', 10);
insert into luminarias values('09', 10);
insert into luminarias values('10', 10);

-- Luminaria: Bloco B 1° PAV
insert into luminarias values('01', 11);
insert into luminarias values('02', 11);
insert into luminarias values('03', 11);
insert into luminarias values('04', 11);
insert into luminarias values('05', 11);
insert into luminarias values('06', 11);
insert into luminarias values('07', 11);
insert into luminarias values('08', 11);
insert into luminarias values('09', 11);
insert into luminarias values('10', 11);

-- Luminaria: Bloco B 2° PAV
insert into luminarias values('01', 12);
insert into luminarias values('02', 12);
insert into luminarias values('03', 12);
insert into luminarias values('04', 12);
insert into luminarias values('05', 12);
insert into luminarias values('06', 12);
insert into luminarias values('07', 12);
insert into luminarias values('08', 12);
insert into luminarias values('09', 12);
insert into luminarias values('10', 12);

-- Luminaria: Bloco B 3° PAV
insert into luminarias values('01', 13);
insert into luminarias values('02', 13);
insert into luminarias values('03', 13);
insert into luminarias values('04', 13);
insert into luminarias values('05', 13);
insert into luminarias values('06', 13);
insert into luminarias values('07', 13);
insert into luminarias values('08', 13);
insert into luminarias values('09', 13);
insert into luminarias values('10', 13);

-- Luminaria: Bloco B 4° PAV
insert into luminarias values('01', 14);
insert into luminarias values('02', 14);
insert into luminarias values('03', 14);
insert into luminarias values('04', 14);
insert into luminarias values('05', 14);
insert into luminarias values('06', 14);
insert into luminarias values('07', 14);
insert into luminarias values('08', 14);
insert into luminarias values('09', 14);
insert into luminarias values('10', 14);

-- Luminaria: Bloco B 5° PAV
insert into luminarias values('01', 15);
insert into luminarias values('02', 15);
insert into luminarias values('03', 15);
insert into luminarias values('04', 15);
insert into luminarias values('05', 15);
insert into luminarias values('06', 15);
insert into luminarias values('07', 15);
insert into luminarias values('08', 15);
insert into luminarias values('09', 15);
insert into luminarias values('10', 15);

-- Luminaria: Bloco C 1° PAV
insert into luminarias values('01', 16);
insert into luminarias values('02', 16);
insert into luminarias values('03', 16);
insert into luminarias values('04', 16);
insert into luminarias values('05', 16);
insert into luminarias values('06', 16);
insert into luminarias values('07', 16);
insert into luminarias values('08', 16);
insert into luminarias values('09', 16);
insert into luminarias values('10', 16);

-- Luminaria: Bloco C 2° PAV
insert into luminarias values('01', 17);
insert into luminarias values('02', 17);
insert into luminarias values('03', 17);
insert into luminarias values('04', 17);
insert into luminarias values('05', 17);
insert into luminarias values('06', 17);
insert into luminarias values('07', 17);
insert into luminarias values('08', 17);
insert into luminarias values('09', 17);
insert into luminarias values('10', 17);

-- Luminaria: Bloco C 3° PAV
insert into luminarias values('01', 18);
insert into luminarias values('02', 18);
insert into luminarias values('03', 18);
insert into luminarias values('04', 18);
insert into luminarias values('05', 18);
insert into luminarias values('06', 18);
insert into luminarias values('07', 18);
insert into luminarias values('08', 18);
insert into luminarias values('09', 18);
insert into luminarias values('10', 18);

-- Luminaria: Bloco C 4° PAV
insert into luminarias values('01', 19);
insert into luminarias values('02', 19);
insert into luminarias values('03', 19);
insert into luminarias values('04', 19);
insert into luminarias values('05', 19);
insert into luminarias values('06', 19);
insert into luminarias values('07', 19);
insert into luminarias values('08', 19);
insert into luminarias values('09', 19);
insert into luminarias values('10', 19);

-- Luminaria: Bloco C 5° PAV
insert into luminarias values('01', 20);
insert into luminarias values('02', 20);
insert into luminarias values('03', 20);
insert into luminarias values('04', 20);
insert into luminarias values('05', 20);
insert into luminarias values('06', 20);
insert into luminarias values('07', 20);
insert into luminarias values('08', 20);
insert into luminarias values('09', 20);
insert into luminarias values('10', 20);

-- Luminaria: Bloco D 1° PAV
insert into luminarias values('01', 21);
insert into luminarias values('02', 21);
insert into luminarias values('03', 21);
insert into luminarias values('04', 21);
insert into luminarias values('05', 21);
insert into luminarias values('06', 21);
insert into luminarias values('07', 21);
insert into luminarias values('08', 21);
insert into luminarias values('09', 21);
insert into luminarias values('10', 21);

-- Luminaria: Bloco D 2° PAV
insert into luminarias values('01', 22);
insert into luminarias values('02', 22);
insert into luminarias values('03', 22);
insert into luminarias values('04', 22);
insert into luminarias values('05', 22);
insert into luminarias values('06', 22);
insert into luminarias values('07', 22);
insert into luminarias values('08', 22);
insert into luminarias values('09', 22);
insert into luminarias values('10', 22);

-- Luminaria: Bloco D 3° PAV
insert into luminarias values('01', 23);
insert into luminarias values('02', 23);
insert into luminarias values('03', 23);
insert into luminarias values('04', 23);
insert into luminarias values('05', 23);
insert into luminarias values('06', 23);
insert into luminarias values('07', 23);
insert into luminarias values('08', 23);
insert into luminarias values('09', 23);
insert into luminarias values('10', 23);

-- Luminaria: Bloco E 1° PAV
insert into luminarias values('01', 24);
insert into luminarias values('02', 24);
insert into luminarias values('03', 24);
insert into luminarias values('04', 24);
insert into luminarias values('05', 24);
insert into luminarias values('06', 24);
insert into luminarias values('07', 24);
insert into luminarias values('08', 24);
insert into luminarias values('09', 24);
insert into luminarias values('10', 24);

-- Luminaria: Bloco E 2° PAV
insert into luminarias values('01', 25);
insert into luminarias values('02', 25);
insert into luminarias values('03', 25);
insert into luminarias values('04', 25);
insert into luminarias values('05', 25);
insert into luminarias values('06', 25);
insert into luminarias values('07', 25);
insert into luminarias values('08', 25);
insert into luminarias values('09', 25);
insert into luminarias values('10', 25);

-- Luminaria: Bloco E 3° PAV
insert into luminarias values('01', 26);
insert into luminarias values('02', 26);
insert into luminarias values('03', 26);
insert into luminarias values('04', 26);
insert into luminarias values('05', 26);
insert into luminarias values('06', 26);
insert into luminarias values('07', 26);
insert into luminarias values('08', 26);
insert into luminarias values('09', 26);
insert into luminarias values('10', 26);
