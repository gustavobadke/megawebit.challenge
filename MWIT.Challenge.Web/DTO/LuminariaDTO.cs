﻿using MWIT.Challenge.Domain.Models;

namespace MWIT.Challenge.Web.DTO
{
    public class LuminariaDTO
    {
        public int Id { get; set; }
        public string Descricao { get; set; }
        public string ImagemFilename { get; set; }
        public EstadoLuminariaEnum Estado { get; set; }
    }
}