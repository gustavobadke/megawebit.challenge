﻿using System.Collections.Generic;

namespace MWIT.Challenge.Web.DTO
{
    public class BlocoFullDTO
    {
        public int Id { get; set; }
        public string Descricao { get; set; }

        public IEnumerable<PavimentoFullDTO> Pavimentos { get; set; }
    }
}