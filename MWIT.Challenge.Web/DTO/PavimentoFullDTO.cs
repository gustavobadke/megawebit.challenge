﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MWIT.Challenge.Web.DTO
{
    public class PavimentoFullDTO
    {
        public int Id { get; set; }
        public string Descricao { get; set; }

        public IEnumerable<LuminariaDTO> Luminarias { get; set; }
    }
}
