﻿using MWIT.Challenge.Domain.Models;

namespace MWIT.Challenge.Web.Helpers
{
    public class LevantamentoHelper
    {
        public static string CssClassEstado(EstadoLuminariaEnum estado)
        {
            switch (estado)
            {
                case EstadoLuminariaEnum.Funcionando:
                    return "legenda-funcionando";

                case EstadoLuminariaEnum.Queimada:
                    return "legenda-queimada";

                case EstadoLuminariaEnum.Piscando:
                    return "legenda-piscando";

                case EstadoLuminariaEnum.PendenteDeVerificacao:
                    return "legenda-pendente";

                default:
                    return "legenda-nada";
            }
        }
    }
}