﻿using System;

using System.Linq;
using System.Web.Mvc;
using MWIT.Challenge.Domain.Infra;
using MWIT.Challenge.Domain.Services;
using MWIT.Challenge.Web.Converters;
using MWIT.Challenge.Web.Models;

namespace MWIT.Challenge.Web.Controllers
{
    public class LevantamentosController : Controller
    {
        // GET: Levantementos
        public ActionResult Index()
        {
            using (var ctx = new ChallengeDbContext())
            {
                return View(new LevantamentoService(ctx).Listar());
            }
        }

        // GET: Levantementos/Details/5
        public ActionResult Details(int id)
        {
            var vm = new LevantamentosDetailViewModel();

            using (var ctx = new ChallengeDbContext())
            {
                var levService = new LevantamentoService(ctx);

                var lev = levService.Get(id);

                vm.NomeCliente = lev.NomeCliente;
                vm.NomeEdificio = lev.NomeEdificio;
                vm.Estado = levService.GetEstadoRota(lev);
                vm.Blocos = BlocoConverter.ConvertBlocos(ctx, lev.Levantamentos);
                
                return View(vm);
            }
        }

        // GET: Levantementos/Create
        public ActionResult Create()
        {
            var vm = new LevantamentosCreateViewModel();

            using (var ctx = new ChallengeDbContext())
            {
                vm.Blocos = new BlocosService(ctx).Listar()
                    .Select(p => new SelectListItem { Value = p.Id.ToString(), Text = p.Descricao });
            }

            return View(vm);
        }

        // POST: Levantementos/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(LevantamentosCreateViewModel vm)
        {
            try
            {
                if (!ModelState.IsValid) throw new Exception();

                using (var ctx = new ChallengeDbContext())
                {
                    new LevantamentoService(ctx).AdicionarLevantamento(vm);
                }

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View(vm);
            }
        }

        // GET: Levantementos/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Levantementos/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Levantementos/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Levantementos/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}