﻿using MWIT.Challenge.Web.Models;
using System.Diagnostics;
using System.Web.Mvc;

namespace MWIT.Challenge.Web.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return RedirectToActionPermanent("Index", "Levantamentos");
        }

        public ActionResult Error()
        {
            return View(new ErrorViewModel());
        }
    }
}