﻿using MWIT.Challenge.Domain.Infra;
using MWIT.Challenge.Domain.Models;
using MWIT.Challenge.Domain.Services;
using MWIT.Challenge.Web.Converters;
using System.Web.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace MWIT.Challenge.Web.Controllers
{
    [RoutePrefix("api")]
    public class WebApiController : Controller
    {
        // GET: api/<controller>
        [Route("blocos/{id}")]
        public JsonResult GetBloco(int id)
        {
            BlocoModel bloco;

            using (var ctx = new ChallengeDbContext())
            {
                bloco = new BlocosService(ctx).Get(id, "Pavimentos", "Pavimentos.Luminarias");
            }

            return Json(BlocoConverter.ConvertFullDTO(bloco), JsonRequestBehavior.AllowGet);
        }
    }
}