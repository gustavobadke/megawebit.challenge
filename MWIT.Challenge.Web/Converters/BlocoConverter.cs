﻿using MWIT.Challenge.Domain.Infra;
using MWIT.Challenge.Domain.Models;
using MWIT.Challenge.Web.DTO;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MWIT.Challenge.Web.Converters
{
    public class BlocoConverter
    {
        public static BlocoFullDTO ConvertFullDTO(BlocoModel model)
        {
            return new BlocoFullDTO
            {
                Id = model.Id,
                Descricao = model.Descricao,
                Pavimentos = model.Pavimentos.Select(p =>
                    new PavimentoFullDTO
                    {
                        Id = p.Id,
                        Descricao = p.Descricao,
                        Luminarias = p.Luminarias.Select(l => new LuminariaDTO
                        {
                            Id = l.Id,
                            Descricao = l.Descricao
                        })
                    })
            };
        }
        
        public static ICollection<BlocoFullDTO> ConvertBlocos(ChallengeDbContext ctx, ICollection<LevantamentoModel> levantamentos)
        {
            var bids = levantamentos.Select(lev => lev.BlocoId);

            var blocos = ctx.Blocos
                .Include("Pavimentos")
                .Include("Pavimentos.Luminarias")
                .Where(p => bids.Any(bId => bId == p.Id))
                .ToList();

            var list = new List<BlocoFullDTO>();


            BlocoFullDTO blocoDto;
            PavimentoFullDTO pavDto;
            LuminariaDTO lumDto;
            
            foreach (var bloco in blocos)
            {
                blocoDto = new BlocoFullDTO {
                    Id = bloco.Id,
                    Descricao = bloco.Descricao,
                    Pavimentos = new List<PavimentoFullDTO>()
                };

                foreach (var pav in bloco.Pavimentos.OrderByDescending(p=>p.Id))
                {
                    pavDto = new PavimentoFullDTO { Id = pav.Id, Descricao = pav.Descricao, Luminarias = new List<LuminariaDTO>() };

                    foreach (var lum in pav.Luminarias)
                    {
                        lumDto = new LuminariaDTO { Id = lum.Id, Descricao = lum.Descricao };
                        var lev = levantamentos.FirstOrDefault(p => p.LuminariaId == lum.Id);
                        if(lev != null)
                        {
                            lumDto.Estado = lev.Estado;
                            lumDto.ImagemFilename = lev.ImagemFilename;
                        }
                        
                        ((ICollection<LuminariaDTO>)pavDto.Luminarias).Add(lumDto);
                    }

                    ((ICollection<PavimentoFullDTO>)blocoDto.Pavimentos).Add(pavDto);
                }

                list.Add(blocoDto);
            }

            return list;
        }
    }
}