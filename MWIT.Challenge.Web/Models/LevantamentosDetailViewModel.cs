﻿using MWIT.Challenge.Web.DTO;
using System.Collections.Generic;

namespace MWIT.Challenge.Web.Models
{
    public class LevantamentosDetailViewModel
    {
        public int Id { get; set; }
        public string NomeEdificio { get; set; }
        public string NomeCliente { get; set; }
        public string Estado { get; set; }
        
        public ICollection<BlocoFullDTO> Blocos { get; set; }
    }
}