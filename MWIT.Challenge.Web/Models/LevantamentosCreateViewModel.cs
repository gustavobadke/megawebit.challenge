﻿using MWIT.Challenge.Domain.Interfaces;
using MWIT.Challenge.Domain.Models;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace MWIT.Challenge.Web.Models
{
    public class LevantamentosCreateViewModel : IAdicionarLevantamento
    {
        [Required]
        public string NomeEdificio { get; set; }

        [Required]
        public string NomeCliente { get; set; }

        public ICollection<int> BlocosId { get; set; }
        public ICollection<int> PavimentosId { get; set; }

        [Required]
        public ICollection<int> LuminariasId { get; set; }

        public IEnumerable<SelectListItem> Blocos { get; set; }
    }
}