﻿'use strict';

var app = angular.module('app', [
    "ngResource"
]);

app.value('BaseApi', 'http://localhost:83/api/');

app.factory('BlocoRes', ['$resource', 'BaseApi', function ($resource, BaseApi) {
    return $resource(BaseApi + 'blocos/:id', {id:'@id'});
}]);

app.controller('LevantamentoController', ['$scope', 'BlocoRes', function($scope, BlocoRes){

    $scope.addBlocoId = 1; //Bloco A
    $scope.blocos = [];

    $scope.adicionarBloco = function () {
        BlocoRes.get({ id: $scope.addBlocoId }).$promise.then(function (data) {
            console.log(data);
            $scope.blocos.push(data);
        });
    }
    
}])