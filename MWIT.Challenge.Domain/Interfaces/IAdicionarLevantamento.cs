﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MWIT.Challenge.Domain.Interfaces
{
    public interface IAdicionarLevantamento
    {
        string NomeEdificio { get; set; }
        string NomeCliente { get; set; }
        ICollection<int> LuminariasId { get; set; }
    }
}
