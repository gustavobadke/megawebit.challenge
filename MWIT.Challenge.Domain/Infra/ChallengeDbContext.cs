﻿
using MWIT.Challenge.Domain.Infra.Migrations;
using MWIT.Challenge.Domain.Models;
using System.Data.Entity;

namespace MWIT.Challenge.Domain.Infra
{
    public class ChallengeDbContext : DbContext
    {
        public ChallengeDbContext() : base("ChallengeDbContext")
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<ChallengeDbContext, Configuration>());
        }
        
        public DbSet<LuminariaModel> Luminarias { get; set; }
        public DbSet<BlocoModel> Blocos { get; set; }
        public DbSet<PavimentoModel> Pavimentos { get; set; }
        public DbSet<LevantamentoModel> Levantamentos { get; set; }
        public DbSet<RotaLevantamentoModel> RotasLevantamentos { get; set; }
    }
}