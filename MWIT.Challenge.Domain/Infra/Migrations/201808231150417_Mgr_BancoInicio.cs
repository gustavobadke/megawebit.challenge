namespace MWIT.Challenge.Domain.Infra.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Mgr_BancoInicio : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.blocos",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Descricao = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.pavimentos",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Descricao = c.String(),
                        Bloco_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.blocos", t => t.Bloco_Id)
                .Index(t => t.Bloco_Id);
            
            CreateTable(
                "dbo.luminarias",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Descricao = c.String(),
                        Pavimento_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.pavimentos", t => t.Pavimento_Id)
                .Index(t => t.Pavimento_Id);
            
            CreateTable(
                "dbo.levantamentos",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Descricao = c.String(),
                        ImagemFilename = c.String(),
                        Estado = c.Int(nullable: false),
                        LuminariaId = c.Int(nullable: false),
                        BlocoId = c.Int(nullable: false),
                        PavimentoId = c.Int(nullable: false),
                        RotaId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.blocos", t => t.BlocoId, cascadeDelete: true)
                .ForeignKey("dbo.luminarias", t => t.LuminariaId, cascadeDelete: true)
                .ForeignKey("dbo.pavimentos", t => t.PavimentoId, cascadeDelete: true)
                .ForeignKey("dbo.rotasLevatamentos", t => t.RotaId, cascadeDelete: true)
                .Index(t => t.LuminariaId)
                .Index(t => t.BlocoId)
                .Index(t => t.PavimentoId)
                .Index(t => t.RotaId);
            
            CreateTable(
                "dbo.rotasLevatamentos",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        NomeEdificio = c.String(),
                        NomeCliente = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.levantamentos", "RotaId", "dbo.rotasLevatamentos");
            DropForeignKey("dbo.levantamentos", "PavimentoId", "dbo.pavimentos");
            DropForeignKey("dbo.levantamentos", "LuminariaId", "dbo.luminarias");
            DropForeignKey("dbo.levantamentos", "BlocoId", "dbo.blocos");
            DropForeignKey("dbo.luminarias", "Pavimento_Id", "dbo.pavimentos");
            DropForeignKey("dbo.pavimentos", "Bloco_Id", "dbo.blocos");
            DropIndex("dbo.levantamentos", new[] { "RotaId" });
            DropIndex("dbo.levantamentos", new[] { "PavimentoId" });
            DropIndex("dbo.levantamentos", new[] { "BlocoId" });
            DropIndex("dbo.levantamentos", new[] { "LuminariaId" });
            DropIndex("dbo.luminarias", new[] { "Pavimento_Id" });
            DropIndex("dbo.pavimentos", new[] { "Bloco_Id" });
            DropTable("dbo.rotasLevatamentos");
            DropTable("dbo.levantamentos");
            DropTable("dbo.luminarias");
            DropTable("dbo.pavimentos");
            DropTable("dbo.blocos");
        }
    }
}
