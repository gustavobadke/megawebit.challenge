﻿using System.ComponentModel.DataAnnotations.Schema;

namespace MWIT.Challenge.Domain.Models
{
    [Table("luminarias")]
    public class LuminariaModel
    {
        public int Id { get; set; }
        public string Descricao { get; set; }

        public PavimentoModel Pavimento { get; set; }
    }
}