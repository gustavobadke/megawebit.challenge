﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace MWIT.Challenge.Domain.Models
{
    [Table("pavimentos")]
    public class PavimentoModel
    {
        public int Id { get; set; }
        public string Descricao { get; set; }

        public BlocoModel Bloco { get; set; }

        public ICollection<LuminariaModel> Luminarias { get; set; }
    }
}