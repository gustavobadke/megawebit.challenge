﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace MWIT.Challenge.Domain.Models
{
    [Table("blocos")]
    public class BlocoModel
    {
        public int Id { get; set; }
        public string Descricao { get; set; } 

        public ICollection<PavimentoModel> Pavimentos { get; set; }
    }
}