﻿namespace MWIT.Challenge.Domain.Models
{
    /*
     * Na análise indica criar uma tabela para esse estado de conservação da
     * luminária, no entanto, acredito que um enum dá conta da necessidade
     * */

    public enum EstadoLuminariaEnum : int
    {
        Funcionando = 1,
        Queimada = 2,

        /* Sei que está fora da análise, mas achei que esse novo estado
        * seria conveniente, caso não, é só tirar.
        * */
        Piscando = 3,
        PendenteDeVerificacao = 4
    }
}