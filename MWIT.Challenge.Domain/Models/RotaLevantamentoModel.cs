﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace MWIT.Challenge.Domain.Models
{
    [Table("rotasLevatamentos")]
    public class RotaLevantamentoModel
    {
        public int Id { get; set; }
        public string NomeEdificio { get; set; }
        public string NomeCliente { get; set; }
        public ICollection<LevantamentoModel> Levantamentos { get; set; }
    }
}