﻿using System.ComponentModel.DataAnnotations.Schema;

namespace MWIT.Challenge.Domain.Models
{
    [Table("levantamentos")]
    public class LevantamentoModel
    {
        public int Id { get; set; }
        public string Descricao { get; set; }
        public string ImagemFilename { get; set; }
        public EstadoLuminariaEnum Estado { get; set; }

        public int LuminariaId { get; set; }
        public int BlocoId { get; set; }
        public int PavimentoId { get; set; }
        public int RotaId { get; set; }

        public RotaLevantamentoModel Rota { get; set; }
        public LuminariaModel Luminaria { get; set; }
        public BlocoModel Bloco { get; set; }
        public PavimentoModel Pavimento { get; set; }
    }
}