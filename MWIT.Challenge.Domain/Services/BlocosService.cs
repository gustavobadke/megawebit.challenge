﻿
using MWIT.Challenge.Domain.Infra;
using MWIT.Challenge.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MWIT.Challenge.Domain.Services
{
    public class BlocosService
    {
        ChallengeDbContext ctx;

        public BlocosService(ChallengeDbContext context)
        {
            ctx = context;      
        }

        public ICollection<BlocoModel> Listar()
        {
            return ctx.Blocos.ToList();
        }

        public BlocoModel Get(int id, params string[] includes)
        {
            var query = ctx.Blocos.AsNoTracking();

            foreach (var include in includes)
            {
                query = query.Include(include);
            }
            
            return query.FirstOrDefault(p=>p.Id == id);
        }
    }
}
