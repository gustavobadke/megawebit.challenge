﻿using MWIT.Challenge.Domain.Infra;
using MWIT.Challenge.Domain.Interfaces;
using MWIT.Challenge.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MWIT.Challenge.Domain.Services
{
    public class LevantamentoService
    {
        ChallengeDbContext ctx;

        public LevantamentoService(ChallengeDbContext context)
        {
            ctx = context;
        }

        public void AdicionarLevantamento(IAdicionarLevantamento param) {

            var rota = new RotaLevantamentoModel
            {
                NomeCliente = param.NomeCliente,
                NomeEdificio = param.NomeEdificio,
                Levantamentos = new List<LevantamentoModel>()
            };

            var luminarias = ctx.Luminarias
                    .Include("Pavimento")
                    .Include("Pavimento.Bloco")
                    .Where(l => param.LuminariasId.Any(lumId => lumId == l.Id))
                    .ToList();
            
            foreach (var lum in luminarias)
            {
                rota.Levantamentos.Add(new LevantamentoModel
                {
                    BlocoId = lum.Pavimento.Bloco.Id,
                    PavimentoId = lum.Pavimento.Id,
                    LuminariaId = lum.Id,
                    Descricao = lum.Descricao,
                    Estado = EstadoLuminariaEnum.PendenteDeVerificacao
                });
            }

            ctx.RotasLevantamentos.Add(rota);
            ctx.SaveChanges();
        }

        public string GetEstadoRota(RotaLevantamentoModel lev)
        {
            if (lev.Levantamentos.All(p => p.Estado == EstadoLuminariaEnum.PendenteDeVerificacao))
                return "Não Iniciada";

            if (lev.Levantamentos.Any(p => p.Estado == EstadoLuminariaEnum.PendenteDeVerificacao))
                return "Em Andamento";

            if (lev.Levantamentos.All(p => p.Estado != EstadoLuminariaEnum.PendenteDeVerificacao))
                return "Finalizada";

            return "";
        }

        public IEnumerable<RotaLevantamentoModel> Listar()
        {
            return ctx.RotasLevantamentos.ToList();
        }

        public RotaLevantamentoModel Get(int id)
        {
            return ctx.RotasLevantamentos
                .Include("Levantamentos")
                .FirstOrDefault(p=>p.Id == id);
        }

    }
}
